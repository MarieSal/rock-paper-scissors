var express = require('express');
var router = express.Router();
var User = require('..//model/UserModel');
var Match = require('..//model/MatchModel');
var VerifyToken = require('../controller/VerifyTokenController');

// Handle view user info
exports.view = function (req, res) {
    Match.findById(req.params.match_id, function (err, match) {
        if (err)
            res.send(err);
        res.json({
            message: 'Match details loading..',
            data: match
        });
    });
};


exports.match = function (req, res) {
    try {
        VerifyToken.verify(req, res);
        User.findOne({ email: req.body.playerOneEmail }, function (err, playerOne) {
            if (playerOne == null) {
                return res.status(200).send({ error: 'Usuario no encontrado' });
            }
            let isOpenMatch = false;
            let winner = "";
            let matchId = "";
            if (playerOne.openMatch) {
                Match.findOne({ $or:[ {playerOneEmail: req.body.playerOneEmail}, 
                                    {playerTwoEmail: req.body.playerOneEmail} ], 
                                isFinished: false}, function (err, match){
                    if (match == null) {
                        return res.status(200).send({ error: 'No hay partida abierta' });
                    }

                    match.cardTwo = req.body.card;
                    match.isFinished = true;

                    if (match.cardOne !== match.cardTwo) {
                        if (match.cardOne === "Piedra") {
                            if (match.cardTwo === "Papel") {
                                winner = "playerTwo";
                                match.winner = match.playerTwoEmail;
                            }
                            if (match.cardTwo === "Tijera") {
                                match.winner = match.playerOneEmail;
                                winner = "playerOne";                            
                            }
                        }
                        if (match.cardOne === "Papel") {
                            if (match.cardTwo === "Piedra") {
                                match.winner = match.playerOneEmail;
                                winner = "playerOne";
                            }
                            if (match.cardTwo === "Tijera") {
                                winner = "playerTwo";       
                                match.winner = match.playerTwoEmail;

                            }
                        }
                        if (match.cardOne === "Tijera") {
                            if (match.cardTwo === "Piedra") {
                                match.winner = match.playerTwoEmail;
                                winner = "playerTwo";
                            }
                            if (match.cardTwo === "Papel") {
                                match.winner = match.playerOneEmail;
                                winner = "playerOne";                            
                            }
                        }
                    }
                    else {
                        winner = "draw";
                        match.winner = "Empate!";
                    }

                    match.save();
                    res.json({
                        message: 'Match updated!',
                        winner: winner,
                        data: match
                    });
                });
                
            }
            else {
                let match = new Match();
                match.playerOneEmail = req.body.playerOneEmail;
                match.cardOne = req.body.card;
                match.playerTwoEmail = req.body.playerTwoEmail;
                match.isFinished = false;
                match.save(function (err) {
                    if (err) { 
                        res.json(err);
                    }
                res.json({
                        message: 'New match created!',
                        data: match
                    });
                });
                matchId = match._id;
                isOpenMatch = true;
            }
            playerOne.openMatch = isOpenMatch;
            playerOne.opponentEmail = req.body.playerTwoEmail;
            playerOne.playerNumber = "playerOne";
            playerOne.lastMatchId = matchId;
            if (winner === "playerOne") {
                playerOne.points++;
            }

            if (!isOpenMatch) {
                playerOne.opponentEmail = "";
                playerOne.playerNumber = "";
            }

            playerOne.save();
            User.findOne({ email: req.body.playerTwoEmail }, function (err, playerTwo) {
                if (playerTwo == null) {
                    return res.status(200).send({ error: 'Usuario no encontrado' });
                }
                if (winner === "playerTwo") {
                    playerTwo.points++;
                }
                playerTwo.openMatch = isOpenMatch;
                playerTwo.opponentEmail = req.body.playerOneEmail;
                playerTwo.lastMatchId = matchId;
                playerTwo.playerNumber = "playerTwo";
                if (!isOpenMatch) {
                    playerTwo.opponentEmail = "";
                    playerTwo.playerNumber = "";
                }
                playerTwo.save();
            });
            
        });
    }
    catch (ex) {
        console.log(ex);
    }

}