var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('../model/UserModel');
var Match = require('../model/MatchModel');
var config = require('../config');
var jwt = require("jsonwebtoken");
//var VerifyToken = require('./VerifyToken');


exports.login = function (req, res) {
    
    User.findOne({ email: req.body.email }, function (err, user) {
        if (user == null) {
            return res.status(200).send({ auth: false, token: null, error: 'Usuario no encontrado' });
        }
        if (user.password != req.body.password) {
            return res.status(200).send({ auth: false, token: null, error: 'Contraseña incorrecta' });
        }
        var token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        user.token = token;
        user.save();
        
        res.status(200).send({ auth: true, token: token, userToken: user.token, 
                openMatch: user.openMatch, opponentEmail: user.opponentEmail, 
                lastGameResult: user.lastGameResult, playerNumber: user.playerNumber,
                playerName: user.name, lastMatchId: user.lastMatchId });
    })
}

/*exports.getLoggedInUser = function (req, res, next) {
//router.get('/me', VerifyToken, function(req, res, next) {
    verifyToken(req, res, next);
    User.findById(req.userId, { password: 0 }, function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
    
        res.status(200).send(user);
    });
};*/