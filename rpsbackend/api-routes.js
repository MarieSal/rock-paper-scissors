// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import user controller
var userController = require('./controller/UserController');

// Import auth controller
var authController = require('./controller/AuthController');

// Import match controller
var matchController = require('./controller/MatchController');

// User routes
router.route('/users')
    .get(userController.index)
    .post(userController.new);
router.route('/users/:user_id')
    .get(userController.view)
    .patch(userController.update)
    .put(userController.update)
    .delete(userController.delete);
// Export API routes

router.route('/login')
    .post(authController.login);
    
router.route('/match')
    .post(matchController.match);
router.route('/match/:match_id')
    .get(matchController.view);
    
module.exports = router;