var mongoose = require('mongoose');
// Setup schema
var MatchSchema = mongoose.Schema({
    playerOneEmail: {
        type: String
    },
    playerTwoEmail: {
        type: String
    },
    cardOne: {
      type: String
    },
    cardTwo: {
        type: String
    },
    winner: {
        type: String
    },
    isFinished: {
        type: Boolean
    }
});
// Export Match model
var Match = module.exports = mongoose.model('match', MatchSchema);
module.exports.get = function (callback, limit) {
    Match.find(callback).limit(limit);
}