// User.js
/*var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({  
  name: String,
  email: String,
  password: String
});
mongoose.model('User', UserSchema);
module.exports = mongoose.model('User');
*/
// userModel.js
var mongoose = require('mongoose');
// Setup schema
var UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
      type: String,
      required: true
    },
    token: {
        type: String
    },
    opponentEmail: {
        type: String
    }, 
    openMatch: {
        type: Boolean
    },
    points: {
        type: Number,
        default : 0
    },
    lastGameResult: {
        type: String,
        default: ""
    },
    playerNumber: {
        type: String,
        default: ""
    },
    lastMatchId: {
        type: String,
        default: ""
    }
});
// Export User model
var User = module.exports = mongoose.model('user', UserSchema);
module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}