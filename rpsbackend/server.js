// Import express
let express = require('express');
// Import Body parser
let bodyParser = require('body-parser');
// Import Mongoose
let mongoose = require('mongoose');
// Import jwt
var jwt = require("jsonwebtoken");

// Import the library:
var cors = require('cors');

// Initialize the app
let app = express();

// Setup server port
var port = process.env.PORT || 3001;
// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express 2'));
// Launch app to listen to specified port
app.listen(port, function () {
     console.log("Running RestHub on port " + port);
});


// Set up a whitelist and check against it:
var whitelist = ['http://localhost:3000', 'http://localhost:3001']
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}

// Then pass them to cors:
app.use(cors());

// Import routes
let apiRoutes = require("./api-routes");

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
   extended: true
}));
app.use(bodyParser.json());


// Use Api routes in the App
app.use('/api', apiRoutes);


// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/resthub', { useNewUrlParser : true });
var db = mongoose.connection;
