import React, { Component } from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import './App.css';
// Components
/* import { Menu } from "./components/Menu"; */
// Components
import { Api, Home, Login, Game, StartGame, FinishGame, Logout, Register } from "./containers";


class App extends Component {
	  //redirects list
	
  	render () {
    	return (
			<BrowserRouter>
				<>
				<Route exact path="/" component={Home} />
				<Route path="/api" component={Api} />
				<Route path="/login" component={Login} />
				<Route path="/game" component={Game} />
				<Route path="/startgame" component={StartGame} />
				<Route path="/finishgame" component={FinishGame} />
				<Route path="/logout" component={Logout} />
				<Route path="/register" component={Register} />
				</>
			</BrowserRouter>
			
    	)
  	}
}
export default App