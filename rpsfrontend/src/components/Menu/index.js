// Dependencies
// import React from "react";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

// Assets
import "./styles.scss";

class Menu extends Component {
    constructor(props) {
        super(props);
        this.checkUserLoggedIn = this.checkUserLoggedIn.bind(this);
    }

state = {
   isUserLoggedIn: 'false',
   isUserMatchOpen: 'false',
   playerName: ''
};

checkUserLoggedIn = checkUserLoggedIn => {
    this.setState({
        isUserLoggedIn : localStorage.getItem('auth') === 'true',
        isUserMatchOpen : localStorage.getItem('openMatch') === 'true'
    });
}


componentDidMount() {
   // this.obtenerUnShow();
    this.checkUserLoggedIn();
    this.state.playerName = localStorage.getItem("playerName");
}




render() {
return  (
    <nav className="Menu">
        <NavLink to="/" exact className="item" activeClassName="active">
            Inicio
        </NavLink>
            

        { this.state.isUserLoggedIn ?
            <>
            { this.state.isUserMatchOpen ?
                <NavLink to="/game" className="item" activeClassName="active">
                    Partida
                </NavLink> 
                :                
                <NavLink to="/api" className="item" activeClassName="active">
                    Partida
                </NavLink>                
                
            }
            <NavLink to="/logout" className="item itemRight" activeClassName="active">
                <img src={require("./../../assets/img/logout-icon.png")} className="logoutIcon" alt=""/>
                <div className="playerNameClass">
                    { this.state.playerName }
                </div>
                {/* Logout */}
            </NavLink>
            </>
            :
            <>
            <NavLink to="/register" className="item" activeClassName="active">
                Registro
            </NavLink>
            <NavLink to="/login" className="item" activeClassName="active">
                Ingresar
            </NavLink>
            
            </> }


    </nav>
);
}}

/* const Menu = () => (
<nav className="Menu">
  <NavLink to="/" exact className="item" activeClassName="active">
    Home
  </NavLink>
  <NavLink to="/login" className="item" activeClassName="active">
    Login
  </NavLink>
  <NavLink to="/api" className="item" activeClassName="active">
    Oponentes
  </NavLink>
</nav>
); */

export { Menu };