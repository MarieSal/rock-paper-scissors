// Dependencies
import React from "react";

// Assets
import "./styles.scss";

const Logo = ({ children, className }) => (
  <div className="logoCircle">
      <img src={require("./../../assets/img/home-logo.png")} alt=""/>
      {/* <img src={require("./../../assets/img/home-logo-2.png")} alt=""/> */}
          </div>
);

export { Logo };
