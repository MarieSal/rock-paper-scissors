// Dependencies
import React from "react";

// Assets
import "./styles.scss";

const Container = ({ children, className }) => (
  <div className={`Container${className && " " + className}`}>
    <div className="wrapper">{children}</div>
  </div>
);

export { Container };
