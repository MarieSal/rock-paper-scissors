// Dependencies
import React, { Component } from "react";

// Components
import { Container } from "../../components/Container";
import { Menu } from "../../components/Menu";

// Utilities
import { URL_API, endpoints } from "../../utils";

// Assets
import "./styles.scss";

class FinishGame extends Component {
	constructor(props) {
		super(props);
		this.getMatch = this.getMatch.bind(this);	
		this.matchId = localStorage.getItem("matchId");	
	}
	
	state = {
		match: null,
		cardOne: "",
		playerOneEmail: "",
		playerTwoEmail: "",
		winner: ""
	}

	componentDidMount() {
		this.getMatch();
	}

	getMatch = () =>
        fetch(`${URL_API}/${endpoints.match}/` + this.matchId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token
            }
		})
		.then(response => response.json())
        .then(response => {
			this.state.match = response.data;
			let finalWinner = "";
			if (response.data.winner === "Empate!") {
				finalWinner = response.data.winner;
			}
			if (response.data.winner === localStorage.getItem("email")) {
				finalWinner = "Ganaste!";
			}
			if (response.data.winner === localStorage.getItem("opponentEmail")) {
				finalWinner = "Perdiste :(";
			}
			
			this.setState({
				match: response.data,
				cardOne: response.data.cardOne,
				cardTwo: response.data.cardTwo,
				playerOne: response.data.playerOneEmail,
				playerTwo: response.data.playerTwoEmail,
				winner: finalWinner
			});

			//localStorage.removeItem("email");
			//localStorage.removeItem("matchId");
			/* localStorage.removeItem("playerNumber");
			localStorage.removeItem("opponentEmail");
			localStorage.removeItem("playerName"); */
			//localStorage.removeItem("playedCard");
			localStorage.setItem("openMatch", "false");
		})
        .catch(error => {
            console.log("error", error);
        });


	render() {

		return (
			<>
				<Menu />
				<Container className="gameContainer">
					<h1 className="title">PARTIDA FINALIZADA! </h1>
					<div className="versusContainer">
						<div className="gamePlayer">
							{ this.state.playerOne } <br/>
							{ this.state.cardOne } 
						</div>
						<span className="versus">VS.</span>
						<div className="gamePlayer">
							{ this.state.playerTwo }<br/>
							{ this.state.cardTwo } 
						</div>
					</div>
					<h2>Resultado: { this.state.winner }</h2>
				</Container>
			</>
		);
	}
}

export { FinishGame };
