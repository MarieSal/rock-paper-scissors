// Dependencies
import React, { Component } from "react";

// Components
import { Menu } from "../../components/Menu";


// Utilities
import { URL_API, endpoints } from "../../utils";

// Assets
import "./styles.scss";

class Game extends Component {
    constructor(props) {
        super(props);
        this.token = localStorage.getItem('token');
        this.selectCard = this.selectCard.bind(this);
        if (localStorage.getItem("openMatch") !== null 
                && localStorage.getItem("openMatch") !== "" 
                && localStorage.getItem("openMatch") !== "undefined"
                && localStorage.getItem("openMatch") === "true"
                && localStorage.getItem("playerNumber") === "playerOne") {
            //redirect
            const { history } = this.props;
            history.push('/startgame');
        }
    }
    state = {
    };

    selectCard(event) {
        let selectedCard = "";
        if (event.target.getAttribute("id") == null) {
            selectedCard = event.target.parentElement.getAttribute("id");
        }
        else {
            selectedCard = event.target.getAttribute("id");
        }
        const jsonbody = JSON.stringify({
            playerOneEmail: localStorage.getItem('email'),
            playerTwoEmail: localStorage.getItem('opponentEmail'),
            card: selectedCard
        });

        console.log(localStorage.getItem('opponentEmail'));
        if (localStorage.getItem('email') != null 
                    && localStorage.getItem('opponentEmail') != null  
                    && selectedCard != null ) {
            fetch(`${URL_API}/${endpoints.match}`, {
                body: jsonbody,
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.token
                }
            })
            .then(resp => resp.json())        
            .then(resp => {
                console.log(resp);
                localStorage.setItem("playedCard", resp.data.cardOne);
                localStorage.setItem("matchId", resp.data._id);
                const { history } = this.props;
                if (resp.message === "New match created!"){
                    localStorage.setItem("playedCard", resp.data.cardOne);
                    localStorage.setItem("openMatch", "true");
                    localStorage.setItem("playerNumber", "playerOne");
                    //redirect
                    history.push('/startgame');
                }
                else {
                    if (resp.message === "Match updated!") {
                        //redirect
                        history.push('/finishgame');
                    }
                }
            })
            .catch(error => {
                this.setState({
                    error: "Ha habido un error. Contacte al administrador."
                });
            });            
        }
        
    };

    render() {
        return (
            <>
                <Menu />
                <div className="gameWrapper">
                    <h1 className="title chooseCardTitle">ELIGE TU CARTA</h1>
                    <div className="cardWrapper">
                        <div className="playCard" onClick={this.selectCard} id="Piedra">
                            <div className="cardPic">
                            <img src={require("./../../assets/img/rock-icon.png")} alt=""/>
                            </div>
                            <h3>Piedra</h3>
                        </div>
                        <div className="playCard" onClick={this.selectCard} id="Papel">
                            <div className="cardPic">
                            <img src={require("./../../assets/img/paper-icon.png")} alt=""/>
                            </div>
                            <h3>Papel</h3>
                        </div>
                        <div className="playCard" onClick={this.selectCard} id="Tijera">
                        <div className="cardPic">
                            <img src={require("./../../assets/img/scissors-icon.png")} alt=""/>
                            </div>
                            <h3>Tijera</h3>
                        </div>
                    </div>
                </div>
            </>
        )
    }

}

export { Game };