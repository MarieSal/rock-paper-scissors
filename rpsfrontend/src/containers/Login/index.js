// Dependencies
import React, { Component } from "react";

// Components
import { Container } from "../../components/Container";
import { Menu } from "../../components/Menu";
import { Logo } from "../../components/Logo";

// Utilities
import { URL_API, endpoints } from "../../utils";

// Assets
import "./styles.scss";

class Login extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    state = {
        email: "",
        password: "",
        error: null
    };

    onSubmit = loginEvent => {
        loginEvent.preventDefault();
        const { history } = this.props;
        const jsonbody = JSON.stringify({
            email: this.state.email,
            password: this.state.password
        });
        console.log('jsonbody >>> ' + jsonbody);
        fetch(`${URL_API}/${endpoints.login}`, {
            body: jsonbody,
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(resp => resp.json())        
        .then(resp => {
            this.setState({
                error: resp.error//"No se conectó con la api"
            });
            // setter
            localStorage.setItem('auth', resp.auth);
            localStorage.setItem('token', resp.token);
            localStorage.setItem('email', this.state.email);
            if (resp.openMatch !== null && resp.openMatch !== "" && resp.openMatch !== "undefined") {
              localStorage.setItem('openMatch', resp.openMatch);
              localStorage.setItem('matchId', resp.lastMatchId);
              localStorage.setItem('opponentEmail', resp.opponentEmail);
              localStorage.setItem('lastGameResult', resp.lastGameResult);
              localStorage.setItem('playerNumber', resp.playerNumber);
              localStorage.setItem('playerName', resp.playerName);
            }
            
            if (resp.auth) {
                //redirect
                history.push('/api');
            }
        })
        .catch(error => {
            this.setState({
                error: "Ha habido un error. Contacte al administrador."
            });
        });
    };

    handleChange = handleChangeEvent => {
        const { name, value } = handleChangeEvent.target;
        this.setState({ [name]: value });
    }

  render() {
    const { email, password, error } = this.state;

    return (
      <>
        <Menu />
        <Container className="Login">
        <Logo />
          {/* <h1 className="title">INICIAR SESIÓN</h1> */}

          <form onSubmit={this.onSubmit}>
            <input
              type="text"
              id="email"
              name="email"
              placeholder="Usuario"
              value={email}
              onChange={this.handleChange}
            />
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Contraseña"
              value={password}
              onChange={this.handleChange}
            />

            <input type="submit" value="Iniciar sesión" />
          </form>

          {/* {email}
          <br />
          {password} */}
          {/* {error} */}

          {error !== null ? <p className="errorMessage"> <div className="excMark"> ! </div> {error}</p> : null}

        </Container>
      </>
    );
  }
}

export { Login };
