// Dependencies
import React, { Component } from "react";

// Components
import { Menu } from "../../components/Menu";
import { Container } from "../../components/Container";
import "./styles.scss";

class Logout extends Component {
    constructor(props) {
        super(props);
    }
    
    componentDidMount() {
        localStorage.clear();
        
    }
    
    render() {
        return (
            <>
                <Menu />
                <Container className="Login">
                <img className="logoutImage" src={require("./../../assets/img/logout-icon-3.png")} alt=""/>
                    <h1 className="logoutTitle">Sesión cerrada, hasta luego!</h1>
                </Container>
            </>
        )
    }

}

export { Logout };