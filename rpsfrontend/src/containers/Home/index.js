// Dependencies
import React, { Component } from "react";

// Components
import { Menu } from "../../components/Menu";
import { Container } from "../../components/Container";
import { Logo } from "../../components/Logo";
// import "./styles.scss";

class Home extends Component {
    constructor(props) {
        super(props);
        this.routeChange = this.routeChange.bind(this);
        this.routeChangeMatch = this.routeChangeMatch.bind(this);
        this.checkUserLoggedIn = this.checkUserLoggedIn.bind(this);
    }
    state = {
        isUserLoggedIn: 'false'
     };
     
     checkUserLoggedIn = checkUserLoggedIn => {
        this.setState({
            isUserLoggedIn : localStorage.getItem('auth') === 'true'
        });
    }
    
    
    componentDidMount() {
       // this.obtenerUnShow();
       this.checkUserLoggedIn();
    }
    /* onSubmit = redirEvent => {
        redirEvent.preventDefault();
        const { history } = this.props;   
                //redirect
                history.push('/login');
    }; */
    routeChange(){
        this.props.history.push('/login');
        }
    routeChangeMatch(){
        this.props.history.push('/api');
        }

    render() {
        return (
            <>
                <Menu />
                <Container className="Login">
          <Logo />
          { this.state.isUserLoggedIn ?
          <input type="button" 
          onClick={this.routeChangeMatch}
            
            value="Partida"
         />  :
          <input type="button" 
                         onClick={this.routeChange}
                           
                           value="Iniciar Sesión"
                        />  
          }

        </Container>
            </>
        )
    }

}

export { Home };