// Dependencies
import React, { Component } from "react";

// Components
import { Menu } from '../../components/Menu';
// import { Container } from '../../components/Container';

// Utilities
import { URL_API, endpoints } from "../../utils";

// Assets
import "./styles.scss";

class Api extends Component {
    constructor(props) {
        super(props);
        this.token = localStorage.getItem('token');
        this.selectUser = this.selectUser.bind(this);
    }

    state = {
        users: null,
        lastGameResult: localStorage.getItem("lastGameResult") 
    };

        
    componentDidMount() {
        if (localStorage.getItem("openMatch") !== null 
                && localStorage.getItem("openMatch") !== "" 
                && localStorage.getItem("openMatch") !== "undefined"
                && localStorage.getItem("openMatch") === "true") {
            //redirect
            const { history } = this.props;
            history.push('/game');
        }
        else {
            this.getUsers().then(apiResponse => console.log(apiResponse));
        }
    }

    selectUser(event) {
        if (event.target.getAttribute("id") == null) {
            localStorage.setItem('opponentEmail', event.target.parentElement.getAttribute("id"));
        }
        else {
            localStorage.setItem('opponentEmail', event.target.getAttribute("id"));
        }
        //redirect
        const { history } = this.props;
        history.push('/game');
    }

    /*obtenerUnShow = () =>
        fetch(`${URL_API}/${endpoints.singlesearch}?q=users`)
        .then(response => response.json())
        .then(response => this.setState({ name: response.name }))
        .catch(error => console.log("error", error));*/

    getUsers = () =>
        fetch(`${URL_API}/${endpoints.users}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token
            },
        })
        .then(response => response.json())
        .then(response => {
            this.setState({
                users: response.data.map(item => ({
                    id: item._id,
                    email: item.email,
                    name: item.name,
                    openMatch: item.openMatch,
                    points: item.points
                }))
            });
            
            var i = 0;
            var usersAux = [];
            for (i = 0; i < this.state.users.length; i++) {
                if (this.state.users[i].email !== localStorage.getItem('email')
                        && !this.state.users[i].openMatch) {
                    usersAux.push(this.state.users[i]);
                }                
            }
            this.setState({ 
                users: usersAux
            });
        })
        .catch(error => {
            console.log("error", error);
        });

        

  render() {
    const { users } = this.state;

    return (
        <>
            <Menu />
            { this.state.lastGameResult !== null &&
                this.state.lastGameResult !== "" &&
                this.state.lastGameResult !== "undefined" ?
                <>
                <div className="lastGame">
                    <h3 className="title lastGameResultTitle">Resultado de la última partida</h3>
                    <h4>{ this.state.lastGameResult }</h4>
                </div>
                <br/>
                </> : null}
                

            <h1 className="title chooseMatchTitle">ELIGE A TU OPONENTE</h1>
            <div className="userWrapper">
                {users &&
                    users.map(user => (
                    <div className="userCard" key={user.email} onClick={this.selectUser} id={user.email}>
                        <div className="userPic">
                            <img src={require("./../../assets/img/user-icon-2.png")} alt=""/>
                        </div>
                        <h3>{user.name}</h3>
                        {/* <p>{user.email}</p>  */}
                        {/* <p>{user.password}</p> */}
                    </div>
                ))}
                </div>
        </>
    );
  }
}

export { Api };
