// Dependencies
import React, { Component } from "react";

// Components
import { Container } from "../../components/Container";
import { Menu } from "../../components/Menu";

// Assets
import "./styles.scss";

class StartGame extends Component {
	constructor(props) {
        super(props);
        this.opponent = localStorage.getItem('opponentEmail');
        this.playedCard = localStorage.getItem('playedCard');
    }


	render() {

		return (
			<>
				<Menu />
				<Container className="startContainer">
					<h1 className="title">PARTIDA INICIADA CON:</h1>
					<h4>{ this.opponent }</h4>
					
					<h1 className="title">JUGASTE:</h1>
					<h4>{ this.playedCard }</h4>
				</Container>
			</>
		);
	}
}

export { StartGame };
