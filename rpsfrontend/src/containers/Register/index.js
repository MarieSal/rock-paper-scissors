// Dependencies
import React, { Component } from "react";

// Components
import { Container } from "../../components/Container";
import { Menu } from "../../components/Menu";
import { Logo } from "../../components/Logo";

// Utilities
import { URL_API, endpoints } from "../../utils";

// Assets
import "./styles.scss";

class Register extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    state = {
        email: "",
        password: "",
        name: "",
        error: null
    };

    onSubmit = registerEvent => {
      registerEvent.preventDefault();
        const { history } = this.props;
        const jsonbody = JSON.stringify({
            email: this.state.email,
            password: this.state.password,
            name: this.state.name
        });
        console.log('jsonbody >>> ' + jsonbody);
        fetch(`${URL_API}/${endpoints.users}`, {
            body: jsonbody,
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(resp => resp.json())        
        .then(resp => {
            this.setState({
                error: null,
                name: '',
                email: '',
                password: ''
            });
            history.push('/');
            // setter
        })
        .catch(error => {
            this.setState({
                error: "Ha habido un error. Contacte al administrador."
            });
        });
    };

    handleChange = handleChangeEvent => {
        const { name, value } = handleChangeEvent.target;
        this.setState({ [name]: value });
    }

  render() {
    const { name, email, password, error } = this.state;

    return (
      <>
        <Menu />
        <Container className="Login">
        <Logo />
          {/* <h1 className="title">INICIAR SESIÓN</h1> */}

          <form onSubmit={this.onSubmit}>
            <input
              type="text"
              id="name"
              name="name"
              placeholder="Nombre"
              value={name}
              onChange={this.handleChange}
            />
            <input
              type="text"
              id="email"
              name="email"
              placeholder="Email de usuario"
              value={email}
              onChange={this.handleChange}
            />
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Contraseña"
              value={password}
              onChange={this.handleChange}
            />

            <input type="submit" value="Registro" />
          </form>

          {/* {email}
          <br />
          {password} */}
          {/* {error} */}

          {error !== 'undefined' && error !== null ? <p className="errorMessage"> <div className="excMark"> ! </div> {error}</p> : null}

        </Container>
      </>
    );
  }
}

export { Register };
